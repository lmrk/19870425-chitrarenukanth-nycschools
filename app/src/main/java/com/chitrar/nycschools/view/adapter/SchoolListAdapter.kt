package com.chitrar.nycschools.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chitrar.nycschools.databinding.ItemSchoolViewBinding
import com.chitrar.nycschools.model.NycSchool

class SchoolListAdapter(var schoolList: List<NycSchool>, private val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<SchoolListAdapter.SchoolNameViewHolder>() {

    class SchoolNameViewHolder(private val itemSchoolViewBinding: ItemSchoolViewBinding) :
        RecyclerView.ViewHolder(itemSchoolViewBinding.root) {
        fun bindData(school: NycSchool) {
            itemSchoolViewBinding.tvSchoolName.text = school.schoolName?:""
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolNameViewHolder {
        return SchoolNameViewHolder(
            ItemSchoolViewBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolNameViewHolder, position: Int) {
        holder.bindData(school = schoolList[position])
        holder.itemView.setOnClickListener{
            onItemClickListener.onItemClick(schoolList[position].dbn)
        }
    }

    override fun getItemCount(): Int = schoolList.size

    interface OnItemClickListener {
        fun onItemClick(dbn: String?)
    }
}