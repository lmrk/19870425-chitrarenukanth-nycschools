package com.chitrar.nycschools.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NycSchool(val dbn: String?,
                     @SerializedName("school_name")
                     val schoolName: String?) : Parcelable

@Parcelize
data class NycSchoolDetail(val dbn: String?,
                           @SerializedName("school_name")
                           val schoolName: String?,
                           @SerializedName("num_of_sat_test_takers")
                           val satTestTakers: String?,
                           @SerializedName("sat_critical_reading_avg_score")
                           val satReadingScore: String?,
                           @SerializedName("sat_math_avg_score")
                           val satMathScore: String?,
                           @SerializedName("sat_writing_avg_score")
                           val satWritingScore: String?): Parcelable