package com.chitrar.nycschools.viewmodel;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.chitra.nycschools.viewmodel.NYCActivityViewModel;
import com.chitrar.nycschools.model.NycSchool;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class NYCActivityViewModelTest {
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();
    private NYCActivityViewModel viewModel;
    @Mock
    Observer<List<NycSchool>> observer;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        viewModel = new NYCActivityViewModel();
        viewModel.getNycSchoolListObservable().observeForever(observer);
    }

    @Test
    public void testNull(){
        assertNotNull(viewModel.getNycSchoolLists());
        assertNull(viewModel.getNycSchoolDetail());
    }

    @Test
    public void testSuccesss(){
        viewModel.setNycSchoolLists(new ArrayList<>(Arrays.asList(new NycSchool("mn78", "Tsst jhj"))));
        assertNotNull(viewModel.getNycSchoolLists());
    }
}
